\documentclass{article}

\usepackage[utf8]{inputenc}

\usepackage{titling}
\newcommand{\subtitle}[1]{%
  \posttitle{%
    \par\end{center}
    \begin{center}\large#1\end{center}
    \vskip0.5em}%
}

\begin{document}

\title{Trasporto affidabile multi-percorso}
\subtitle{Laboratorio di Programmazione di Rete a.a. 2011/2012}
\author{
  Alessandro Ghedini \texttt{aghedini@cs.unibo.it} \\
  Gaber Ayoubi \texttt{ayoubi@cs.unibo.it}
}
\maketitle

\begin{abstract}
Si vuole realizzare un sistema per il trasferimento di un flusso di dati tra
due processi tramite una rete inaffidabile a "percorsi multipli", secondo le
specifiche fornite.

Il sistema prevede due processi \textbf{Sender} e \textbf{Receiver} collegati
rispettivamente ad un \textbf{ProxySender} e un \textbf{ProxyReceiver} tramite
TCP. I due proxy comunicano tra di loro usando 3 diversi canali UDP passanti per
un \textbf{Ritardatore} che si occupa di simulare l'inaffidabilità della rete.

La finalità del progetto è implementare i due proxy in maniera tale da
assicurare una comunicazione affidabile tra il \textbf{Sender} e il
\textbf{Receiver}.
\end{abstract}

\section{Protocollo}

\subsection{Pacchetti}

Il protocollo implementato prevede l'uso di pacchetti formati da un header di
dimensione $PKT\_HDRSIZE$ e un body di dimensione variabile, con dimensione
$PKT\_MAXSIZE$.

L'header è così formato:

\begin{itemize}
  \item $seq$ 4 byte contenenti un intero senza segno in endianess di rete
  rappresentante il numero di sequenza del del pacchetto. Viene usato per
  garantire che i dati trasmessi arrivino a destinazione in ordine.

  \item $type$ 1 byte contenente un carattere con valore fisso 'B'. Viene usato
  per differenziare i pacchetti generati dai due proxy (con tipo 'B') da quelli
  generati dal \textbf{Ritardatore} (con tipo 'I') usati per simulare messaggi
  ICMP.

  \item $size$ 4 byte contenenti un intero senza segno in endianess di rete
  rappresentante la lunghezza del body del pacchetto.
\end{itemize}

\subsection{Trasmissione}

La comunicazione comincia nel momento in cui il \textbf{Sender} si connette al
\textbf{ProxySender} e inizia ad inviare dati.

Mano a mano che il \textbf{ProxySender} riceve dati tramite TCP, pacchetti con
$seq$ incrementale vengono creati incapsulando i dati ricevuti, e inviati
tramite UDP verso il \textbf{Ritardatore} che deciderà se scartarli, ritardarli
o lasciarli passare. Il canale del \textbf{Ritardatore} usato per l'invio dei
pacchetti viene deciso usando una politica round-robin per distribuire il
carico uniformemente.

I pacchetti così inviati vengono accodati in una lista usata successivamente
per la conferma dell'avvenuta ricezione dei dati. Nel caso in cui tale conferma
non arrivi entro un determinato periodo di tempo ($QUEUE\_TIMEOUT$), il
pacchetto viene rinviato, mentre nel caso in cui la conferma arrivi in tempo,
il pacchetto viene rimosso dalla lista. Inoltre il \textbf{Ritardatore} può
comunicare la perdita di un pacchetto simulando un pacchetto ICMP usando il
valore 'I' nel campo $type$ dell'header del pacchetto e come $seq$ il $seq$ del
pacchetto scartato, nel qual caso \textbf{ProxySender} si occupa di rispedirlo
subito (a meno che la sua ricezione non sia già stata confermata, ad esempio se
il primo invio era stato ritardato), e ne resetta il timer.

Nel caso di avvenuta ricezione di un pacchetto, il \textbf{ProxyReceiver} si
occuppa di accodarlo ordinatamente in una lista usata per il mantenimento
dell'ordine dei dati. Inoltre si occupa di inviare un pacchetto di conferma
(ACK) verso il \textbf{ProxySender} (passando per il\textbf{Ritardatore}) avente
come $seq$ il $seq$ del pacchetto ricevuto, e come size il valore $0$.

L'invio verso il \textbf{Receiver} viene implementato usando una variabile
($queue\_next$) contenente il primo $seq$ "utile", ovvero il primo seq da
inviare per mantenere i dati ordinati.

Una volta disconnesso il \textbf{Sender}, il \textbf{ProxySender} invia un
normale pacchetto con $size$ pari a $0$ (normalmente confermato e accodato dal
\textbf{ProxyReceiver}, e rinviato dal \textbf{ProxySender} nel caso di mancata
conferma).

A questo punto il \textbf{ProxySender} e il \textbf{ProxyReceiver} continuano a
comunicare finchè tutti i dati non sono stati confermati, dopodichè il
\textbf{ProxySender} si metterà in attesa di una connessione da un nuovo
\textbf{Sender}, mentre il \textbf{ProxyReceiver}, dopo aver finito di inviare
i dati al \textbf{Receiver} attenderà qualche secondo e poi termina la sua
esecuzione. Il timeout finale è necessario nel caso il cui la conferma del
pacchetto finale venga perduta.

\subsection{Controllo di flusso}

Per evitare di congestionare la rete e fare un uso eccessivo di memoria viene
fatto uso di una rudimentale \textit{sliding window} di dimensione prefissata.

Le code dei due proxy vengono implementate in modo da avere un numero
massimo di elementi ($QUEUE\_MAXSIZE$).

Nel caso in cui la coda del \textbf{ProxySender} venga riempita (ovvero nel
caso in cui i pacchetti vengano aggiunti più velocemente di quanto non vengano
confermati e rimossi), la comunicazione con il \textbf{Sender} viene interrotta 
(ovvero il \textbf{ProxySender} smette di ricevere dati) per dare modo alla
coda di svuotarsi.

Inoltre il \textbf{ProxySender} si assicura di inviare pacchetti con $seq$
compreso tra $queue\_next$ e $queue\_next + QUEUE\_MAXSIZE$ in modo tale da
mantenere il numero di pacchetti della code del \textbf{ProxyReceiver} sotto
$QUEUE\_MAXSIZE$. Fare questo controllo direttamente sul \textbf{ProxySender}
evita di dover inviare inutilmente pacchetti che verrebbero comunque scartati
dal \textbf{ProxyReceiver}.

\section{Dettagli di implementazione}

Sia il \textbf{ProxySender} che il \textbf{ProxyReceiver} fanno uso di I/O
multiplexing (come da specifica del progetto) senza l'utilizzo di thread
aggiuntivi.

Le code dei due proxy sono implementate utilizzando doubly-linked list
circolari, in modo da tale che sia l'accesso in testa che in coda abbia
complessità $O(1)$ (ovviamente lo scorrimento della coda rimane $O(n)$). La
dimensione massima delle code ($QUEUE\_MAXSIZE$) è 250 (ovvero ci possono essere
al massimo 250 pacchetti nella finestra di invio).

Per quanto riguarda i pacchetti, la dimensione dell'header ($PKT\_HDRSIZE$) è 9
byte, mentre la dimensione massima del body ($PKT\_MAXSIZE$) è 65KB. Nonostante
la frammentazione in pacchetti Ethernet, questa si è dimostrata essere la
dimensione ideale da un punto di vista prestazionale.

Infine come timeout di rinvio ($QUEUE\_TIMEOUT$), viene utilizzato il valore di
0.2 secondi. Con valori superiori a quello scelto, è risultato che il consumo
di CPU scende leggermente, ma la latenza di rinvio dei singoli paccheti aumenta,
con il risultato che il tempo totale di invio aumenta a sua volta. Con valori
inferiori il consumo di CPU aumenta notevolmente mentre la latenza cala
leggermente. Il valore scelto è quindi un compromesso ottimale (almeno secondo
i test eseguiti) tra tempi totali di invio e consumo di CPU.

\section{Consumo di risorse e prestazioni}

Per verificare l'affidabilità del progetto sono stati inviati file con contenuti
casuali di diverse dimensioni. I file di test sono stati generati utilizzando il
comando $dd$ specificando le dimensioni 10MB, 20MB, 50MB, 100MB e 1GB, e
utilizzando $/dev/urandom$ come sorgente per il loro contenuto. I dati ricevuti
sono poi stati confrontati con i dati di partenza utilizzando il comando $cmp$.

\subsection{CPU}

Le misurazioni del consumo di CPU sono state effettuate utilizzando il comando
$top$ durante il trasferimento di un file da 1GB in locale.

Per il \textbf{ProxySender} la percentuale di CPU occupata varia fino ad un
massimo del 6\%, mentre per il \textbf{ProxyReceiver} arriva ad un massimo del
4\% su un sistema con processore dual core da 2.1GHz.

\subsection{Memoria}

Le misurazioni del consumo di memoria sono anch'esse state effettuate durante
il trasferimento di un file da 1GB in locale sia utilizzando il comando $top$,
sia utilizzando il comando $valgrind$ per verificare che tutta la memoria
allocata venga liberata correttamente (ovvero che non siano presenti memory
leaks).

$top$ riporta un consumo di circa il 0.4\% sia per \textbf{ProxySender} che per
\textbf{ProxyReceiver} si un sistema con 4GB di memoria, con picchi di circa
18MB di memoria virtuale utilizzata.

$valgrind$ riporta invece che tutta la memoria allocata è stata correttamente
liberata ("All heap blocks were freed -- no leaks are possible"). Inoltre
riporta anche che nessun errore di accesso alla memoria (es. lettura/scrittura
di memoria non inizializzata) è stato commesso dai due proxy durante
l'esecuzione.

\subsection{Bandwidth e tempo di invio}

Il tempo di invio (misurato con il comando $time$) e la banda consumata
(misurata grazie all'output del \textbf{Ritardatore}) aumentanono quasi
linearmente con l'aumentare della quantità di dati inviati (ovvero la dimensione
dei file).

\end{document}
