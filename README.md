Trasporto affidabile multi-percorso
===================================

## BUILD

```bash
$ make
```

## USAGE

* Start the `ritardatore`:

```bash
$ ./ghini
```

* Start the receiver and its proxy:

```bash
$ nc -l -p 64000
$ ./proxy_receiver
```

* Start the sender and its proxy:

```bash
$ nc localhost 59000
$ ./proxy_sender
```
