CFLAGS  ?= -Wall -Wextra -pedantic -ansi -g -Iinclude

COMMON_OBJS   = src/tcp.o src/udp.o src/queue.o src/misc.o
SENDER_OBJS   = src/proxy_sender.o $(COMMON_OBJS)
RECEIVER_OBJS = src/proxy_receiver.o $(COMMON_OBJS)

GHINI_OBJS  = src/ghini/Util.o src/ghini/Ritardatore.o

.PHONY: all docs clean style

all: proxy_sender proxy_receiver ghini

proxy_sender: $(SENDER_OBJS)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

proxy_receiver: $(RECEIVER_OBJS)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

ghini: $(GHINI_OBJS)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS) -lm

src/%.o: %.c
	$(CC) $(CFLAGS) -c $<

docs:
	pdflatex docs/relazione.tex

clean:
	rm -f src/*.o src/ghini/*.o
	rm -f proxy_sender proxy_receiver ghini
	rm -f relazione.*

style:
	astyle -t -a -e -f -p -H -k3 -n -U src/*.c include/*.h
