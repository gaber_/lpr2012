#include "proxy.h"

static struct sockaddr_in udp_serv;

static char *serialize_pkt(pkt_t *pkt, ssize_t *msg_len) {
	char *msg;
	size_t i = 0;
	uint32_t seq, size;

	seq  = htonl(pkt -> seq);
	size = htonl(pkt -> size);

	*msg_len = PKT_HDRSIZE + pkt -> size;

	msg = malloc(*msg_len);
	check_alloc(msg, "malloc()");

	memcpy(msg + i, &seq, sizeof(seq));
	i += sizeof(seq);

	memcpy(msg + i, &pkt -> type, sizeof(pkt -> type));
	i += sizeof(pkt -> type);

	memcpy(msg + i, &size, sizeof(size));
	i += sizeof(size);

	if (pkt -> size > 0)
		memcpy(msg + i, pkt -> body, pkt -> size);

	return msg;
}

static pkt_t *deserialize_pkt(char *msg) {
	pkt_t *pkt = malloc(sizeof(pkt_t));
	check_alloc(pkt, "malloc()");

	memcpy(&pkt -> seq, msg, sizeof(pkt -> seq));
	msg += sizeof(pkt -> seq);
	pkt -> seq = ntohl(pkt -> seq);

	memcpy(&pkt -> type, msg, sizeof(pkt -> type));
	msg += sizeof(pkt -> type);

	memcpy(&pkt -> size, msg, sizeof(pkt -> size));
	msg += sizeof(pkt -> size);
	pkt -> size = ntohl(pkt -> size);

	if (pkt -> type != 'B')
		return pkt;

	if (pkt -> size > 0) {
		pkt -> body = malloc(pkt -> size);
		check_alloc(pkt -> body, "malloc()");

		pkt -> body = memcpy(pkt -> body, msg, pkt -> size);
	}

	return pkt;
}

int udp_init(int *udp_fd, int *send_fd, int port) {
	int rc;

	udp_serv.sin_family		= AF_INET;
	udp_serv.sin_addr.s_addr	= INADDR_ANY;
	udp_serv.sin_port		= htons(port);

	*udp_fd = socket(AF_INET, SOCK_DGRAM, 0);
	do {
		rc = bind(*udp_fd, (struct sockaddr *) &udp_serv,
			sizeof(struct sockaddr));
	} while (check_error(rc, "bind()"));

	*send_fd = socket(AF_INET, SOCK_DGRAM, 0);

	return 0;
}

ssize_t udp_recv(int fd, pkt_t **pkt) {
	ssize_t len;
	char msg[PKT_HDRSIZE + PKT_MAXSIZE];

	do {
		len = recvfrom(fd, msg, PKT_HDRSIZE + PKT_MAXSIZE,
			0, NULL, 0);
	} while (check_error(len, "recvfrom()"));

	*pkt = deserialize_pkt(msg);

	if (*pkt == NULL)
		return -1;

	return len;
}

ssize_t udp_send(int fd, pkt_t *pkt, int port_base) {
	struct sockaddr_in d;

	int port;
	static int turn = 0;

	ssize_t len;
	char *msg = serialize_pkt(pkt, &len);

	if (msg == NULL)
		return -1;

	turn = (turn + 1) % 3;
	port = port_base + turn;

	d.sin_family      = AF_INET;
	d.sin_addr.s_addr = inet_addr(R_HOST);
	d.sin_port        = htons(port);

	do {
		len = sendto(fd, msg, len, 0, (struct sockaddr *) &d, sizeof(d));
	} while (check_error(len, "sendto()"));

	free(msg);

	return len;
}
