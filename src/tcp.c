#include "proxy.h"

static uint32_t seq_next = 0;
static struct sockaddr_in tcp_serv;

int tcp_init_sender(int *fd, int port) {
	int rc;

	tcp_serv.sin_family = AF_INET;
	tcp_serv.sin_addr.s_addr = INADDR_ANY;
	tcp_serv.sin_port = htons(port);

	do {
		*fd = socket(AF_INET, SOCK_STREAM, 0);
	} while (check_error(*fd, "socket()"));

	do {
		rc = bind(*fd, (struct sockaddr *) &tcp_serv,
			sizeof(struct sockaddr));
	} while (check_error(rc, "bind()"));

	do {
		rc = listen(*fd, 1);
	} while (check_error(rc, "listen()"));

	return 0;
}

int tcp_init_receiver(int *fd, char *host, int port) {
	int rc;

	tcp_serv.sin_family = AF_INET;
	tcp_serv.sin_addr.s_addr = inet_addr(host);
	tcp_serv.sin_port = htons(port);

	do {
		*fd = socket(AF_INET, SOCK_STREAM, 0);
	} while (check_error(*fd, "socket()"));

	do {
		rc = connect(*fd, (struct sockaddr *) &tcp_serv, sizeof(tcp_serv));
	} while (check_error(rc, "connect()"));

	return 0;
}

int tcp_accept(int fd, int *conn) {
	do {
		*conn = accept(fd, NULL,0);
	} while (check_error(*conn, "accept()"));

	seq_next = 0;

	return 0;
}

ssize_t tcp_recv(int fd, pkt_t **pkt) {
	int rc;
	ssize_t len;
	char buffer[PKT_MAXSIZE];

	if ((queue_getminseq() + QUEUE_MAXSIZE) <= seq_next)
		return -1;

	do {
		len = recv(fd, buffer, PKT_MAXSIZE, 0);
	} while (check_error(len, "recv()"));

	*pkt = malloc(sizeof(pkt_t));
	check_alloc(*pkt, "malloc()");

	(*pkt) -> type = 'B';
	(*pkt) -> seq  = seq_next++;
	(*pkt) -> size = len;

	do {
		rc = gettimeofday(&(*pkt) -> tv, NULL);
	} while (check_error(rc, "gettimeofday()"));

	(*pkt) -> tv.tv_sec--;

	if (len == 0)
		return len;

	(*pkt) -> body = malloc(len);
	check_alloc((*pkt) -> body, "malloc()");

	(*pkt) -> body = memcpy((*pkt) -> body, buffer, len);

	return len;
}

ssize_t tcp_send(int fd, pkt_t *pkt) {
	ssize_t len;

	do {
		len = send(fd, pkt -> body, pkt -> size, 0);
	} while (check_error(len, "send()"));

	return len;
}

uint32_t tcp_seq_next() {
	return seq_next;
}
