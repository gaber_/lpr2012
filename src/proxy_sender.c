#include "proxy.h"

char R_HOST[16] = "127.0.0.1";

int main(int argc, char *argv[]) {
	int rc;
	int fds[4] = { 0, 0, 0, 0 };
	int fd_flags[4] = { 1, 0, 1, 0 };

	fd_set r_fds, w_fds;
	struct timeval *tv = NULL;

	switch (argc) {
		case 2: strcpy(R_HOST, argv[1]);
	}

	/* initialize TCP */
	tcp_init_sender(&fds[TCP_ACCEPT], I_PORT);

	/* initialize UDP */
	udp_init(&fds[UDP_RECV], &fds[UDP_SEND], C_PORT);

	FD_ZERO(&r_fds);
	FD_ZERO(&w_fds);

	FD_SET(fds[TCP_ACCEPT], &r_fds);

	while ((rc = select(maxfd(fds, 4), &r_fds, &w_fds, NULL, tv)) >= 0) {
		if (FD_ISSET(fds[TCP_ACCEPT], &r_fds)) {
			tcp_accept(fds[TCP_ACCEPT], &fds[TCP_RECV]);

			fd_flags[TCP_ACCEPT] = 0;
			fd_flags[TCP_RECV] = 1;
		}

		if ((fd_flags[TCP_ACCEPT] == 0) &&
		    (FD_ISSET(fds[TCP_RECV], &r_fds))) {
			pkt_t *pkt;
			ssize_t len = tcp_recv(fds[TCP_RECV], &pkt);

			switch (len) {
				case -1:
					fd_flags[TCP_RECV] = 0;
					break;

				case 0:
					/* disconnect */
					close(fds[TCP_RECV]);

					fd_flags[TCP_ACCEPT] = 1;
					fd_flags[TCP_RECV] = 0;

				default:
					/* ok */
					udp_send(fds[UDP_SEND], pkt, R_IPORT);
					queue_push_timer(pkt);

					fd_flags[UDP_SEND] = 1;
					break;
			}

			if (queue_is_full())
				fd_flags[TCP_RECV] = 0;
		}

		if (FD_ISSET(fds[UDP_RECV], &r_fds)) {
			pkt_t *pkt, *tmp;
			udp_recv(fds[UDP_RECV], &pkt);

			switch (pkt -> type) {
				case 'I':
					tmp = queue_getbyseq(pkt -> size);

					if (tmp != NULL) {
						udp_send(fds[UDP_SEND],
							tmp, R_IPORT);

						queue_push_timer(tmp);
					}
					break;

				case 'B':
					queue_remove(pkt);

					if (!queue_is_full() &&
					    !fd_flags[TCP_ACCEPT])
						fd_flags[TCP_RECV] = 1;
					break;
			}

			free(pkt);
		}

		if ((rc == 0) || FD_ISSET(fds[UDP_SEND], &w_fds)) {
			pkt_t *pkt;

			while ((pkt = queue_pop_timer())) {
				udp_send(fds[UDP_SEND], pkt, R_IPORT);
				queue_push_timer(pkt);
			}

			fd_flags[UDP_SEND] = 0;

			queue_gettime(&tv);
		}

		FD_ZERO(&r_fds);
		FD_ZERO(&w_fds);

		FD_SET(fds[UDP_RECV], &r_fds);

		if (fd_flags[TCP_ACCEPT])
			FD_SET(fds[TCP_ACCEPT], &r_fds);

		if (fd_flags[TCP_RECV])
			FD_SET(fds[TCP_RECV], &r_fds);

		if (fd_flags[UDP_SEND])
			FD_SET(fds[UDP_SEND], &w_fds);
	}

	check_error(rc, "select()");

	close(fds[TCP_ACCEPT]);
	close(fds[UDP_SEND]);
	close(fds[UDP_RECV]);

	return 0;
}
