#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

int check_error(int rc, const char *msg) {
	if ((errno == EINTR) && (errno == EAGAIN))
		return 1;

	if (rc < 0) {
		perror(msg);
		exit(-1);
	}

	return 0;
}

int maxfd(int *fds, int count) {
	int i, max = 0;

	for (i = 0; i < count; i++)
		if (fds[i] > max)
			max = fds[i];

	return ++max;
}
