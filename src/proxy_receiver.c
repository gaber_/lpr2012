#include "proxy.h"

char R_HOST[16] = "127.0.0.1";
char O_HOST[16] = "127.0.0.1";

int main(int argc, char *argv[]) {
	int rc;
	int fds[4] = { 0, 0, 0, 0 };
	int fd_flags[4] = { 1, 0, 1, 0 };

	fd_set r_fds, w_fds;
	struct timeval *tv = NULL, close_tv;

	switch (argc) {
		case 3: strcpy(O_HOST, argv[2]);
		case 2: strcpy(R_HOST, argv[1]);
	}

	/* initialize TCP */
	tcp_init_receiver(&fds[TCP_SEND], O_HOST, O_PORT);

	/* initialize UDP */
	udp_init(&fds[UDP_RECV], &fds[UDP_SEND], S_PORT);

	FD_ZERO(&r_fds);
	FD_ZERO(&w_fds);

	FD_SET(fds[UDP_RECV], &r_fds);

	while ((rc = select(maxfd(fds, 4), &r_fds, &w_fds, NULL, tv) > 0)) {
		if (FD_ISSET(fds[UDP_RECV], &r_fds)) {
			pkt_t *pkt, ack;
			udp_recv(fds[UDP_RECV], &pkt);

			switch (pkt -> type) {
				case 'I':
					free(pkt);
					break;

				case 'B':
					/* ok */
					rc = queue_push_order(pkt);

					ack.type = 'B';
					ack.seq  = pkt -> seq;
					ack.size = 0;

					fd_flags[TCP_SEND] = 1;

					udp_send(fds[UDP_SEND],
						&ack, R_OPORT);

					if (rc == -1) {
						if (pkt -> size > 0)
							free(pkt -> body);

						free(pkt);
					}
					break;
			}
		}

		if (FD_ISSET(fds[TCP_SEND], &w_fds)) {
			pkt_t *pkt;

			while ((pkt = queue_pop_order())) {
				if (pkt -> size == 0) {
					close_tv.tv_sec = 0;
					close_tv.tv_usec = CLOSE_TIMEOUT;

					tv = &close_tv;
				} else {
					tcp_send(fds[TCP_SEND], pkt);
					free(pkt -> body);
				}

				free(pkt);
			}

			fd_flags[TCP_SEND] = 0;
		}

		FD_ZERO(&r_fds);
		FD_ZERO(&w_fds);

		if (fd_flags[TCP_SEND])
			FD_SET(fds[TCP_SEND], &w_fds);

		if (fd_flags[UDP_RECV])
			FD_SET(fds[UDP_RECV], &r_fds);

		if (fd_flags[UDP_SEND])
			FD_SET(fds[UDP_SEND], &w_fds);
	}

	check_error(rc, "select()");

	close(fds[TCP_SEND]);
	close(fds[UDP_SEND]);
	close(fds[UDP_RECV]);

	return 0;
}
