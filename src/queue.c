#include "proxy.h"

#define tvcmp(a, b, CMP)					\
	(((a) -> tv_sec == (b) -> tv_sec)    ?			\
	 ((a) -> tv_usec CMP (b) -> tv_usec) :			\
	 ((a) -> tv_sec CMP (b) -> tv_sec))

#define tvsub(a, b, result)					\
	(result) -> tv_sec  = (a) -> tv_sec - (b) -> tv_sec;	\
	(result) -> tv_usec = (a) -> tv_usec - (b) -> tv_usec;	\
								\
	if ((result) -> tv_usec < 0) {				\
		--(result) -> tv_sec;				\
		(result) -> tv_usec += 1000000;			\
	}

#define list_foreach(L, I)					\
	for (I = L; I; I = ((I) -> next == L ? NULL : (I) -> next))

#define list_prepend(L, I)					\
	if ((L)) {						\
		(I) -> prev = (L) -> prev;			\
		(I) -> next = (L);				\
								\
		(I) -> prev -> next = (I);			\
		(I) -> next -> prev = (I);			\
	} else {						\
		(I) -> prev = (I);				\
		(I) -> next = (I);				\
								\
		(L) = (I);					\
	}							\
								\
	(L) = (I);

#define list_remove(L, I)					\
	if (((L) == (I)) && ((L) -> next == (L))) {		\
		(L) = NULL;					\
	} else {						\
		(I) -> next -> prev = (I) -> prev;		\
		(I) -> prev -> next = (I) -> next;		\
								\
		if ((I) == (L))					\
			(L) = (I) -> next;			\
	}

struct timeval sel_tv;

static pkt_t *queue = NULL;
static uint32_t queue_size = 0, queue_next = 0;

int queue_is_full() {
	return (queue_size == QUEUE_MAXSIZE);
}

int queue_push_timer(pkt_t *pkt) {
	int rc;
	struct timeval now;

	do {
		rc = gettimeofday(&now, NULL);
	} while (check_error(rc, "gettimeofday()"));

	pkt -> tv.tv_sec  = now.tv_sec;
	pkt -> tv.tv_usec = now.tv_usec + QUEUE_TIMEOUT;

	list_prepend(queue, pkt);

	queue_size++;

	return 0;
}

int queue_push_order(pkt_t *pkt) {
	pkt_t *iter;

	if (pkt -> seq < queue_next)
		return -1;

	if ((queue == NULL) || (queue -> seq > pkt -> seq)) {
		list_prepend(queue, pkt);
		goto done;
	}

	list_foreach(queue, iter) {
		if (iter -> seq == pkt -> seq)
			return -1;

		if (pkt -> seq < iter -> seq)
			break;
	}

	if (iter == NULL)
		iter = queue;

	pkt -> prev = iter -> prev;
	pkt -> next = iter;

	pkt -> prev -> next = pkt;
	pkt -> next -> prev = pkt;

done:
	queue_size++;

	return 0;
}

pkt_t *queue_pop_timer() {
	int rc;
	pkt_t *last;
	struct timeval now;

	if (queue == NULL)
		return NULL;

	do {
		rc = gettimeofday(&now, NULL);
	} while (check_error(rc, "gettimeofday()"));

	last = queue -> prev;

	if (tvcmp(&last -> tv, &now, >))
		return NULL;

	list_remove(queue, last);

	queue_size--;

	return last;
}

pkt_t *queue_pop_order() {
	pkt_t *head;

	if (queue == NULL)
		return NULL;

	if (queue -> seq != queue_next)
		return NULL;

	head = queue;

	list_remove(queue, head);

	queue_size--;
	queue_next++;

	return head;
}

pkt_t *queue_getbyseq(uint32_t seq) {
	pkt_t *iter;

	list_foreach(queue, iter) {
		if (iter -> seq == seq) {
			list_remove(queue, iter);
			queue_size--;

			return iter;
		}
	}

	return NULL;
}

void queue_remove(pkt_t *pkt) {
	pkt_t *clean = queue_getbyseq(pkt -> seq);

	if (clean) {
		if (clean -> size > 0)
			free(clean -> body);

		free(clean);
	}
}

uint32_t queue_getminseq() {
	pkt_t *iter;
	static uint32_t min = 0;

	if (queue == NULL)
		return tcp_seq_next();

	min = queue -> seq;

	list_foreach(queue, iter) {
		min = iter -> seq < min ? iter -> seq : min;
	}

	return min;
}

void queue_gettime(struct timeval **tv) {
	int rc;
	struct timeval now;

	if (queue == NULL) {
		*tv = NULL;
		return;
	}

	do {
		rc = gettimeofday(&now, NULL);
	} while (check_error(rc, "gettimeofday()"));

	tvsub(&queue -> prev -> tv, &now, &sel_tv);

	if (sel_tv.tv_sec < 0) {
		sel_tv.tv_sec  = 0;
		sel_tv.tv_usec = 0;
	}

	*tv = &sel_tv;
}
