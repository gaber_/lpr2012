#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <netinet/in.h>

#include <sys/socket.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/time.h>

#define C_PORT 60000

#define R_IPORT 61000
#define R_OPORT 62000

#define S_PORT 63000

#define I_PORT 59000

#define O_PORT 64000

extern char R_HOST[];

#define PKT_MAXSIZE 65000
#define PKT_HDRSIZE (sizeof(uint32_t) + sizeof(char) + sizeof(uint32_t))

#define QUEUE_TIMEOUT 200000
#define QUEUE_MAXSIZE 250

#define CLOSE_TIMEOUT 2000000

#define check_alloc(pt, MSG)	\
	if (pt == NULL) {	\
		fprintf(stderr, "%s: out of memory", MSG); \
		exit(-1);	\
	}

typedef struct PKT {
	uint32_t seq;
	char type;
	uint32_t size;
	char *body;

	struct timeval tv;
	struct PKT *prev, *next;
} __attribute((packed)) pkt_t;

enum PROXY_FDS {
	TCP_ACCEPT,
	TCP_RECV,
	TCP_SEND = TCP_RECV,
	UDP_RECV,
	UDP_SEND
};

extern int tcp_init_sender(int *fd, int port);
extern int tcp_init_receiver(int *fd, char *host, int port);
extern int tcp_accept(int fd, int *conn_fd);
extern ssize_t tcp_recv(int fd, pkt_t **pkt);
extern ssize_t tcp_send(int fd, pkt_t *pkt);
extern uint32_t tcp_seq_next();

extern int udp_init(int *udp_fd, int *send_fd, int port);
extern ssize_t udp_recv(int fd, pkt_t **pkt);
extern ssize_t udp_send(int fd, pkt_t *pkt, int port);

extern int queue_is_full();
extern int queue_push_timer(pkt_t *pkt);
extern pkt_t *queue_pop_timer();
extern int queue_push_order(pkt_t *pkt);
extern pkt_t *queue_pop_order();
extern void queue_remove(pkt_t *pkt);
extern uint32_t queue_getminseq();
extern pkt_t *queue_getbyseq(uint32_t seq);
extern void queue_gettime(struct timeval **tv);

extern int check_error(int rc, const char *msg);
extern int maxfd(int *fds, unsigned count);
